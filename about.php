<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css ">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
   
    
</head>

<body>
<div class="container-fluid" style="background-image: url('aboutBG.jpg'); background-repeat: no-repeat; background-size: cover; background-position:top;">
        <div class="row " >
            <div class="col-md-10 mx-auto "> 
                 <nav class="navbar navbar-expand-md  navbar-dark " >
                 <a href="Phomepage.php" class="navbar-brand text-white"><img src="logoavi.png" alt="" style="width:180px;height:40px"></a>
                 <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler bg-danger" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <!-- Navbar links -->
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto">
                   
                <li class="nav-item " >
                    <a href="Phomepage.php" class="nav-link text-light" >Home</a>
                </li>
                    <li class="nav-item ">
                    <a href="about.php" class="nav-link text-light">About</a>
                </li>
                    <li class="nav-item ">
                    <a href="services.php" class="nav-link text-light">Services</a>
                </li>
                    <li class="nav-item ">
                    <a href="chooseme.php" class="nav-link text-light">Why choose me</a>
                </li>
                    <li class="nav-item ">
                    <a href="testimonial.php" class="nav-link text-light">Testimonials</a>
                </li>
                    <li class="nav-item ">
                    <a href="contact.php" class="nav-link text-light">Contact</a>
                </li>
                <a href="quotesform.php">   <button class="btn btn-info">GET A QUOTES</button> </a>
            </ul>
            </div>
            </nav> 

                  <br> <br> <br> <br> <br>
                    <div class="row mt-5">
                        <br> <br> <br> <br>
                        <div class="col-md-12 ">
                            <br> <br> <br> 
                                <h2 style="color:rgb(255, 73, 28);font-weight: bold; "> A Few Words</h2>
                                <h1 style="font-weight: bolder;color:white; font-size: 100px">About Me</h1>
                            <br> <br>  
                        </div>
                    </div>     
            </div>
            </div>
        </div>
    </div>
<br><br>
    <div class="container">
        <div class="row" >
            <div class="col-md-8">
                <br> <br> <br> 
                    <h5 style="color:rgb(243, 40, 13);font-weight: bold ">Exceptional service</h5>
                    <h1 style="font-weight: bolder;color: black">Practical financial advice you can count on</h1>
                <br> 
                <h4 style="font-weight: bold;color: grey">Focus your time and efforts on running your business and leave the accounting to me</h4> 
                <br>
                <p >I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut 
                    elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Duis aute irure dolor in reprehenderit in voluptate
                     velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                      officia deserunt mollit anim id est laborum. </p>
            </div>
            <div class="col-md-4 mt-5 mx-auto card ">
                <img  src="aboutpng1.png" style="width: 350px;height: 350px;" alt="" >
        </div>
        </div>
        <br> <br> <br> <br> <br>
    </div>


    <div class="container-fluid  " style="background-color: rgb(231, 243, 243)">
            <div class="row" >
                <div class="col-md-8 mx-auto">
                    <br> <br> <br> 
                        <h5 style="color:rgb(243, 40, 13);font-weight: bold ;text-align: center">Accounting services</h5>
                        <h1 style="font-weight: bolder;color: black;text-align: center">Accurate record keeping is a key component to the success of your business.</h1>
                    <br> 
                    <h5 style="font-weight: bold;color: rgb(72, 115, 172);text-align: center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</h5> 
                    <br>    
                </div>               
            </div>
            <div class="row ">
                <div class="col-md-4 card mx-auto">
                   <div class="card-body  w-10"> <p class="card-text"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> </div> </div> 
                <div class="col-md-4 card mx-auto"> 
                   <div class="card-body"> Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</div> </div>
            </div>
            <br> <br> <br> <br> <br>
            </div>
            
        </div>
    



    <div class="container-fluid mt-4 " >
        <div class="row">
            <div class="col-md-12  ">
                
                    <div class="container">
                            <br> <br> <br>
                            <div class="row">
                                <h3 style="color: rgb(228, 17, 17);font-weight: bolder;margin: auto">I have been featured in:</h3>
                            </div>
                            <br>

                            <div class="carousel slide  " data-ride="carousel" id="myslider" >
                                    <div class="carousel-inner ">
                                        <div class="carousel-item active ">
                                            
                                            <div class="row  ">
                
                                                <div class="col-3">
                                                    <img src="aboutlogo1.png" alt="nature" class="pic">
                                                </div>
                                                <div class="col-3">
                                                    <img src="aboutlogo2.png" alt="nature" class="pic">
                                                </div>
                                                <div class="col-3">
                                                    <img src="aboutlogo3.png" alt="nature" class="pic">
                                                </div>
                                                <div class="col-3 ">
                                                    <img src="aboutlogo4.png" alt="nature" class="pic">
                                                    
                                                </div>
                                                
                                                
                                               
                                            </div>   
                                        </div>
                                        <div class="carousel-item  ">
                                            
                                            <div class="row ">
                                                <div class="col-3">
                                                    <img src="aboutlogo4.png" alt="nature" class="pic">
                                                </div>
                                                <div class="col-3">
                                                    <img src="aboutlogo3.png" alt="nature" class="pic">
                                                </div>
                                                <div class="col-3">
                                                    <img src="aboutlogo2.png" alt="nature" class="pic">
                                                </div>
                                                <div class="col-3">
                                                    <img src="aboutlogo1.png" alt="nature" class="pic">
                                                </div>
                                                
                                            </div>   
                                    </div>
                    <a href="#myslider" class="carousel-control-prev" data-slide="next">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a href="#myslider" class="carousel-control-next" data-slide="prev">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        
                </div>
            </div>
            <br> <br> <br>
        </div>
    </div>
    </div>
    
    </div>
 
    <div class="container-fluid " style="background-image: url('background.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
     <br> <br> <br>
        <div class="row" >
                <div class="col-md-6 mt-5 mx-auto text-white " >
                     <h5 style="color:rgb(223, 49, 49);font-weight: bold;">  Free Estimation </h5>
                      <h1 style="font-weight: bold"> Request a quote </h1>
                       <p> I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Ut elit tellus, luctus nec ullamcorper mattis. </p>
                </div>
                <div class="mx-auto" style="margin-top: auto" >
                <a href="quotesform.php">   <button class="btn btn-info">GET A QUOTES</button> </a>
                </div>
            
           </div>
           <br> <br> <br>
</div>
     
                    <nav class="navbar navbar-expand-sm">
                        <small>Copyright © 2019 Accountant | Powered by Accountant</small>
                        <ul class="navbar-nav ml-auto" >
                           
                        <li class="nav-item ">
                            <a href="Phomepage.php" class="nav-link " style="color: black">Home</a>
                            </li>
                            <li class="nav-item ">
                            <a href="about.php" class="nav-link " style="color: black">About</a>
                            </li>
                            <li class="nav-item ">
                            <a href="services.php" class="nav-link " style="color: black">Services</a>
                            </li>
                            <li class="nav-item ">
                            <a href="chooseme.php" class="nav-link " style="color: black">Why choose me</a>
                            </li>
                            <li class="nav-item ">
                            <a href="testimonial.php" class="nav-link " style="color: black">Testimonials</a>
                            </li>
                            <li class="nav-item ">
                            <a href="contact.php" class="nav-link " style="color: black">Contact</a>
                            </li>
                        </ul>
                        
    
                    </nav>
   
</body>
</html>