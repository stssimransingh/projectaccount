<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css ">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <script src="https://kit.fontawesome.com/3e6e522206.js"></script>
    
</head>

<body>
<div class="container-fluid" style="background-image: url('choosemeBG.png'); background-repeat: no-repeat; background-size: cover; background-position:top;">
        <div class="row " >
            <div class="col-md-10 mx-auto "> 
                 <nav class="navbar navbar-expand-md  navbar-dark " >
                 <a href="Phomepage.php" class="navbar-brand text-white"><img src="logoavi.png" alt="" style="width:180px;height:40px"></a>
                 <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler bg-danger" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <!-- Navbar links -->
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto">
                   
                <li class="nav-item " >
                    <a href="Phomepage.php" class="nav-link text-light" >Home</a>
                </li>
                    <li class="nav-item ">
                    <a href="about.php" class="nav-link text-light">About</a>
                </li>
                    <li class="nav-item ">
                    <a href="services.php" class="nav-link text-light">Services</a>
                </li>
                    <li class="nav-item ">
                    <a href="chooseme.php" class="nav-link text-light">Why choose me</a>
                </li>
                    <li class="nav-item ">
                    <a href="testimonial.php" class="nav-link text-light">Testimonials</a>
                </li>
                    <li class="nav-item ">
                    <a href="contact.php" class="nav-link text-light">Contact</a>
                </li>
                
                <a href="quotesform.php">   <button class="btn btn-info">GET A QUOTES</button> </a>
            </ul>
            </div>
            </nav> 

                  <br> <br> <br> <br> <br>
                    <div class="row mt-5">
                        <br> <br> <br> <br>
                        <div class="col-md-12 ">
                            <br> <br> <br> 
                                <h2 style="color:rgb(241, 126, 80);font-weight: bold; "> Accurate record keeping</h2>
                                <h1 style="font-weight: bolder;color:white; font-size: 100px">Why choose me</h1>
                            <br> <br>  
                        </div>
                    </div>     
            </div>
            </div>
        </div>
    </div>

<div class="container-fluid " style="background-color: rgb(243, 243, 243)">
    <br> <br> <br>
           <h4 style="text-align: center;font-weight: bold;color:rgb(243, 40, 13)">This is why</h4>
           <h2 style="font-weight: bold;text-align: center "> We should work together</h2>
           <br> <br> <br>
           <div class="container">
            <div class="row  " >
               
                    <div class="btn col-md-4  " >
                            <button type="button" class="btn btn-light  " style="width: 350px;height: 300px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);">
                                <i style="font-size:60px;color:rgb(243, 40, 13)" class="far fa-clock"></i> 
                                <br> <br> 
                         <h3 style="font-weight: bold">Always On Time </h3>
                         <br>
                       <p class="text-secondary"> Click edit button to change this text. Lorem ipsum dolor sit amet </p>
                            </button>
                        </div>
                   
                    <div class="btn col-md-4 " >
                            <button type="button" class="btn btn-light" style="width: 350px;height: 300px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);">
                                <i style="font-size:60px;color:rgb(243, 40, 13)" class="fas fa-check"></i> 
                                <br><br>   
                         <h3 style="font-weight: bold">Hard Working</h3>
                         <br>
                         <p class="text-secondary"> Click edit button to change this text. Lorem ipsum dolor sit amet </p>
                            </button>
                    </div>
                    <div class="btn col-md-4" >
                            <button type="button" class="btn btn-light" style="width: 350px;height: 300px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);">  
                                <i style="font-size:60px;color:rgb(243, 40, 13)" class="far fa-calendar-alt"></i>
                                <br> <br>
                         <h3 style="font-weight: bold">24/7 Availability </h3>
                         <br>
                         <p class="text-secondary"> Click edit button to change this text. Lorem ipsum dolor sit amet </p>
                            </button>
                    </div>
                </div>
       </div>

    <br> <br>
    <div class="container">
        <div class="row  " >
           
                <div class="btn col-md-4  " >
                        <button type="button" class="btn btn-light  " style="width: 350px;height: 300px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);"> 
                            <i style="font-size:60px;color:rgb(243, 40, 13)" class="fas fa-rupee-sign"></i>
                            <br><br>    
                     <h3 style="font-weight: bold">Maximum profitability </h3>
                     <br>
                     <p class="text-secondary"> Click edit button to change this text. Lorem ipsum dolor sit amet </p>
                        </button>
                    </div>
               
                <div class="btn col-md-4 " >
                        <button type="button" class="btn btn-light" style="width: 350px;height: 300px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);"> 
                            <i style="font-size:60px;color:rgb(243, 40, 13)" class="fas fa-question"></i>
                            <br><br>    
                     <h3 style="font-weight: bold">Classified transactions </h3>
                     <br>
                     <p class="text-secondary"> Click edit button to change this text. Lorem ipsum dolor sit amet </p>
                        </button>
                </div>
                <div class="btn col-md-4" >
                        <button type="button" class="btn btn-light" style="width: 350px;height: 300px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);"> 
                            <i style="font-size:60px;color:rgb(243, 40, 13)" class="fas fa-crosshairs"></i>
                            <br><br>     
                     <h3 style="font-weight: bold">Always Accurate DFDFFD </h3>
                     <br>
                     <p class="text-secondary"> Click edit button to change this text. Lorem ipsum dolor sit amet </p>
                        </button>
                </div>
            </div>
   </div>
     <br> <br>
</div>
 

 
    <div class="container-fluid " style="background-image: url('background.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <br> <br> <br>
        <div class="row" >
                <div class="col-md-6 mt-5 mx-auto text-white " >
                     <h5 style="color:rgb(223, 49, 49);font-weight: bold">  Free Estimation </h5>
                      <h1 style="font-weight: bold"> Request a quote </h1>
                       <p> I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Ut elit tellus, luctus nec ullamcorper mattis. </p>
                </div>
                <div class="mx-auto" style="margin-top: auto" >
                <a href="quotesform.php">   <button class="btn btn-info">GET A QUOTES</button> </a>
                </div>
            
           </div>
           <br> <br> <br> <br>
</div>
     
                    <nav class="navbar navbar-expand-sm">
                        <small>Copyright © 2019 Accountant | Powered by Accountant</small>
                        <ul class="navbar-nav ml-auto" >
                           
                        <li class="nav-item ">
                            <a href="Phomepage.php" class="nav-link " style="color: black">Home</a>
                            </li>
                            <li class="nav-item ">
                            <a href="about.php" class="nav-link " style="color: black">About</a>
                            </li>
                            <li class="nav-item ">
                            <a href="services.php" class="nav-link " style="color: black">Services</a>
                            </li>
                            <li class="nav-item ">
                            <a href="chooseme.php" class="nav-link " style="color: black">Why choose me</a>
                            </li>
                            <li class="nav-item ">
                            <a href="testimonial.php" class="nav-link " style="color: black">Testimonials</a>
                            </li>
                            <li class="nav-item ">
                            <a href="contact.php" class="nav-link " style="color: black">Contact</a>
                            </li>
                        </ul>
                        
    
                    </nav>
   
</body>
</html>