<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css ">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
   
    
</head>

<body>
<div class="container-fluid" style="background-image: url('servicesBG.jpg'); background-repeat: no-repeat; background-size: cover; background-position:top;">
        <div class="row " >
            <div class="col-md-10 mx-auto "> 
                 <nav class="navbar navbar-expand-md  navbar-dark " >
                 <a href="Phomepage.php" class="navbar-brand text-white"><img src="logoavi.png" alt="" style="width:180px;height:40px"></a>
                 <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler bg-danger" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <!-- Navbar links -->
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto">
                   
                <li class="nav-item " >
                    <a href="Phomepage.php" class="nav-link text-light" >Home</a>
                </li>
                    <li class="nav-item ">
                    <a href="about.php" class="nav-link text-light">About</a>
                </li>
                    <li class="nav-item ">
                    <a href="services.php" class="nav-link text-light">Services</a>
                </li>
                    <li class="nav-item ">
                    <a href="chooseme.php" class="nav-link text-light">Why choose me</a>
                </li>
                    <li class="nav-item ">
                    <a href="testimonial.php" class="nav-link text-light">Testimonials</a>
                </li>
                    <li class="nav-item ">
                    <a href="contact.php" class="nav-link text-light">Contact</a>
                </li>
                
                <a href="quotesform.php">   <button class="btn btn-info">GET A QUOTES</button> </a>
            </ul>
            </div>
            </nav> 

                  <br> <br> <br> <br> <br>
                    <div class="row mt-5">
                        <br> <br> <br> <br>
                        <div class="col-md-12 ">
                            <br> <br> <br> 
                                <h2 style="color:rgb(241, 126, 80);font-weight: bold; "> Services</h2>
                                <h1 style="font-weight: bolder;color:white; font-size: 100px">What I do</h1>
                            <br> <br>  
                        </div>
                    </div>     
            </div>
            </div>
        </div>
    </div>
<br><br>
   
<div class="container ">
    <div class="row ">
        <div class="col-ms-6  " style="margin-left: 80px;margin-top: 100px">
                <img  src="first.png" style="width: 300px;height: 300px;" alt="" >
        </div>
        <div class="col-md-6 ml-auto text-center ">
                
                    <br>
                    <h1 style="font-weight: bold;color:black;"> Accounting Services</h1> 
                    <br>
                    <H4 style="font-weight: bold;color: rgb(228, 13, 13)">Outsourced Accounting </H4>
                       <p class="text-secondary"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos 
                        sit natus debitis et quaerat cum libero veritatis tempore eum numquam,
                        ab exercitationem fugiat possimus quis, nostrum sed eaque sequi voluptatum?</p>
                        <br> <br>
                    <H4 style="font-weight: bold;color: rgb(228, 13, 13)">Compilations </H4>
                        <p class="text-secondary"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos 
                        sit natus debitis et quaerat cum libero veritatis tempore eum numquam,
                        ab exercitationem fugiat possimus quis, nostrum sed eaque sequi voluptatum?</p>
                        <br> <br> <br>
                     <button class="btn btn-light" style=" box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.3);">KNOW MORE</button>
                     <br> <br> <br> <br> <br>
        </div>
    </div>   
</div>

<div class="container ">
    <div class="row ">
        
        <div class="col-md-6 text-center ">
                
                    <br>
                    <h1 style="font-weight: bold;color:black;">Tax services</h1> 
                    <br>
                    <H4 style="font-weight: bold;color: rgb(228, 13, 13)">Individual tax services</H4>
                       <p class="text-secondary"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos 
                        sit natus debitis et quaerat cum libero veritatis tempore eum numquam,
                        ab exercitationem fugiat possimus quis, nostrum sed eaque sequi voluptatum?</p>
                        <br> <br>

                    <H4 style="font-weight: bold;color: rgb(228, 13, 13)">Corporate & Partnership Tax Services </H4>
                        <p class="text-secondary"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos 
                        sit natus debitis et quaerat cum libero veritatis tempore eum numquam,
                        sit natus debitis et quaerat cum libero veritatis tempore eum numquam,
                        ab exercitationem fugiat possimus quis, nostrum sed eaque sequi voluptatum?</p>

                        <H4 style="font-weight: bold;color: rgb(228, 13, 13)">Estate & Trust Tax Services  </H4>
                        <p class="text-secondary"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos 
                        sit natus debitis et quaerat cum libero veritatis tempore eum numquam,
                        ab exercitationem fugiat possimus quis, nostrum sed eaque sequi voluptatum?</p>
                        <br> <br>            
        </div>
        <div class="col-ms-6 mx-auto " style="margin-top: 80px" >
            <img  src="second.png" style="width: 300px;height: 300px;" alt="" >
            <br> <br> <br> <br> <br> <br>
            <button class="btn btn-light " style="box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.3);margin-left: 80px">KNOW MORE</button>
            <br> <br> <br> <br> 
            
    </div>
                  
    </div>   
</div>

<div class="container ">
    <div class="row ">
        <div class="col-ms-6  " style="margin-left: 80px;margin-top: 100px">
                <img  src="third.png" style="width: 300px;height: 300px;" alt="" >
        </div>
        <div class="col-md-6 ml-auto text-center ">
                
                    <br>
                    <h1 style="font-weight: bold;color:black;">Financial Advisory </h1> 
                    <br>
                    <H4 style="font-weight: bold;color: rgb(228, 13, 13)">Mergers & Acquisitions</H4>
                       <p class="text-secondary"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos 
                        sit natus debitis et quaerat cum libero veritatis tempore eum numquam,
                        ab exercitationem fugiat possimus quis, nostrum sed eaque sequi voluptatum?</p>
                        <br> <br>
                    <H4 style="font-weight: bold;color: rgb(228, 13, 13)">Financial Crisis</H4>
                        <p class="text-secondary"> Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos 
                        sit natus debitis et quaerat cum libero veritatis tempore eum numquam,
                        ab exercitationem fugiat possimus quis, nostrum sed eaque sequi voluptatum?</p>
                        <br> <br> <br>
                     <button class="btn btn-light" style=" box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.3);">KNOW MORE</button>
                     <br> <br> <br> <br> <br>
        </div>
    </div>   
</div>
 
    <div class="container-fluid " style="background-image: url('background.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <br> <br> <br>
        <div class="row" >
                <div class="col-md-6 mt-5 mx-auto text-white " >
                     <h5 style="color:rgb(223, 49, 49);font-weight: bold">  Free Estimation </h5>
                      <h1 style="font-weight: bold"> Request a quote </h1>
                       <p> I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Ut elit tellus, luctus nec ullamcorper mattis. </p>
                </div>
                <div class="mx-auto" style="margin-top: auto" >
                <a href="quotesform.php">   <button class="btn btn-info">GET A QUOTES</button> </a>
                </div>
            
           </div>
           <br> <br> <br> <br>
</div>
     
                    <nav class="navbar navbar-expand-sm">
                        <small>Copyright © 2019 Accountant | Powered by Accountant</small>
                        <ul class="navbar-nav ml-auto" >
                           
                        <li class="nav-item ">
                            <a href="Phomepage.php" class="nav-link " style="color: black">Home</a>
                            </li>
                            <li class="nav-item ">
                            <a href="about.php" class="nav-link " style="color: black">About</a>
                            </li>
                            <li class="nav-item ">
                            <a href="services.php" class="nav-link " style="color: black">Services</a>
                            </li>
                            <li class="nav-item ">
                            <a href="chooseme.php" class="nav-link " style="color: black">Why choose me</a>
                            </li>
                            <li class="nav-item ">
                            <a href="testimonial.php" class="nav-link " style="color: black">Testimonials</a>
                            </li>
                            <li class="nav-item ">
                            <a href="contact.php" class="nav-link " style="color: black">Contact</a>
                            </li>
                        </ul>
                        
    
                    </nav>
   
</body>
</html>