<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css ">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <script src="https://kit.fontawesome.com/3e6e522206.js"></script>
   
    
</head>

<body>
<div class="container-fluid" style="background-image: url('testimonialBG.jpg'); background-repeat: no-repeat; background-size: cover; background-position:top;">
        <div class="row " >
            <div class="col-md-10 mx-auto "> 
                 <nav class="navbar navbar-expand-md  navbar-dark " >
                 <a href="Phomepage.php" class="navbar-brand text-white"><img src="logoavi.png" alt="" style="width:180px;height:40px"></a>
                 <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler bg-danger" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <!-- Navbar links -->
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav ml-auto">
                   
                <li class="nav-item " >
                    <a href="Phomepage.php" class="nav-link text-light" >Home</a>
                </li>
                    <li class="nav-item ">
                    <a href="about.php" class="nav-link text-light">About</a>
                </li>
                    <li class="nav-item ">
                    <a href="services.php" class="nav-link text-light">Services</a>
                </li>
                    <li class="nav-item ">
                    <a href="chooseme.php" class="nav-link text-light">Why choose me</a>
                </li>
                    <li class="nav-item ">
                    <a href="testimonial.php" class="nav-link text-light">Testimonials</a>
                </li>
                    <li class="nav-item ">
                    <a href="contact.php" class="nav-link text-light">Contact</a>
                </li>
                <a href="quotesform.php">   <button class="btn btn-info">GET A QUOTES</button> </a>
            </ul>
            </div>
            </nav> 

                  <br> <br> <br> <br> <br>
                    <div class="row mt-5">
                        <br> <br> <br> <br>
                        <div class="col-md-12 ">
                            <br> <br> <br> 
                                <h2 style="color:rgb(241, 126, 80);font-weight: bold; "> What my clients say</h2>
                                <h1 style="font-weight: bolder;color:white; font-size: 80px;">Testimonials</h1>
                            <br> <br>  
                        </div>
                    </div>     
            </div>
            </div>
        </div>
    </div>

    <div class="container ">
        <br> <br> 
        
        
        <div class="row ">
            <div class="btn col-md-4" >
                    <button type="button" class="btn btn-light" style="width: 350px;height: 350px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.2);">     
                     <h1>  <i style="color: rgb(223, 54, 54)" class="fas fa-quote-left "></i></h1>
                    
                 Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                 <br> <br>
                <h3 style="color: rgb(223, 54, 54)"> Amanda Lee </h3>
                <p> Ceo & Founder Crix </p>
                    </button>
            </div>
            <div class="btn col-md-4"  >
                    <button type="button" class="btn btn-light" style="width: 350px;height: 350px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.2);">
                      <h1> <i style="color: rgb(223, 54, 54)" class="fas fa-quote-left "></i> </h1>
                        Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                        <br> <br>
                       <h3 style="color: rgb(223, 54, 54)"> Adam Cheise</h3>
                       <p> Director at Dynamic </p>
                    </button>
            </div>
            <div class="btn col-md-4" >
                    <button type="button" class="btn btn-light" style="width: 350px;height: 350px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.2);">
                      <h1> <i style="color: rgb(223, 54, 54)" class="fas fa-quote-left "></i> </h1> 
                    Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                        <br> <br>
                       <h3 style="color: rgb(223, 54, 54)"> Adam Cheise</h3>
                       <p> Director at Dynamic </p>
                    </button>
            </div>
    </div>
    <div class="row ">
            <div class="btn col-md-4" >
                    <button type="button" class="btn btn-light" style="width: 350px;height: 350px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.2);">   
                    <h1> <i style="color: rgb(223, 54, 54)" class="fas fa-quote-left "></i> </h1>
                 Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                 <br> <br>
                <h3 style="color: rgb(223, 54, 54)"> Nicole Peterson </h3>
                <p> President at Acme </p>
                    </button>
            </div>
            <div class="btn col-md-4"  >
                    <button type="button" class="btn btn-light" style="width: 350px;height: 350px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.2);">
                      <h1> <i style="color: rgb(223, 54, 54)" class="fas fa-quote-left "></i> </h1>
                        Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                        <br> <br>
                       <h3 style="color: rgb(223, 54, 54)"> Jose Johnson</h3>
                       <p> Ceo at Globex</p>
                    </button>
            </div>
            <div class="btn col-md-4" >
                    <button type="button" class="btn btn-light" style="width: 350px;height: 350px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.2);">    
                        <h1> <i style="color: rgb(223, 54, 54)" class="fas fa-quote-left "></i> </h1>                       
                    Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                        <br> <br>
                       <h3 style="color: rgb(223, 54, 54)"> Benjamin Patel</h3>
                       <p> CFO at Soylent </p>
                    </button>
            </div>
            <br>
    </div>
    <div class="row ">
        
            <div class="btn col-md-4" >
                    <button type="button" class="btn btn-light" style="width: 350px;height: 350px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.2);">     
                        <h1> <i style="color: rgb(223, 54, 54)" class="fas fa-quote-left "></i> </h1>           
                Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                 <br> <br>
                <h3 style="color: rgb(223, 54, 54)"> Rebecca Harrison </h3>
                
                <p> President at Hooli </p>
                    </button>
            </div>
            <div class="btn col-md-4"  >
                    <button type="button" class="btn btn-light" style="width: 350px;height: 350px;box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.2);"> 
                      <h1> <i style="color: rgb(223, 54, 54)" class="fas fa-quote-left "></i> </h1>
                        Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                        <br> <br>
                       <h3 style="color: rgb(223, 54, 54)">Sarah Sanchez</h3>
                       <p> Ceo & Founder Vehement </p>
                    </button>
            </div>
            
    </div>
    </div> 

    <div class="container mt-4 " >
            <div class="row mx-auto">
                <div class="col-md-12 " >
                    
                        <div class="container ">
                                <br> <br> <br>
                                <div class="row ">
                                    <h1 style="color: black;font-weight: bolder;margin: auto">Clients I've helped</h1>
                                </div>
                                <br>
    
                    <div class="carousel slide  " data-ride="carousel" id="myslider" >
                        <div class="carousel-inner ">
                            <div class="carousel-item active ">
                                
                                <div class="row  ">
    
                                    <div class="col-3">
                                        <img src="logo3.jpg" alt="nature" class="pic">
                                    </div>
                                    <div class="col-3">
                                        <img src="logo4.jpg" alt="nature" class="pic">
                                    </div>
                                    <div class="col-3">
                                        <img src="logo5.jpg" alt="nature" class="pic">
                                    </div>
                                    <div class="col-3 ">
                                        <img src="logo2.jpg" alt="nature" class="pic">
                                    </div>
                                    
                                    
                                   
                                </div>   
                            </div>
                            <div class="carousel-item  ">
                                
                                <div class="row ">
                                    <div class="col-3">
                                        <img src="logo5.jpg" alt="nature" class="pic">
                                    </div>
                                    <div class="col-3">
                                        <img src="logo4.jpg" alt="nature" class="pic">
                                    </div>
                                    <div class="col-3">
                                        <img src="logo5.jpg" alt="nature" class="pic">
                                    </div>
                                    <div class="col-3">
                                        <img src="logo2.jpg" alt="nature" class="pic">
                                    </div>
                                    
                                </div>   
                        </div>
                        <a href="#myslider" class="carousel-control-prev" data-slide="next">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a href="#myslider" class="carousel-control-next" data-slide="prev">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            
                    </div>
                </div>
                <br> <br> <br>
            </div>
        </div>
        </div>
        
        </div>
 
    <div class="container-fluid " style="background-image: url('background.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <br> <br> <br>
        <div class="row" >
                <div class="col-md-6 mt-5 mx-auto text-white " >
                     <h5 style="color:rgb(223, 49, 49);font-weight: bold">  Free Estimation </h5>
                      <h1 style="font-weight: bold"> Request a quote </h1>
                       <p> I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                        Ut elit tellus, luctus nec ullamcorper mattis. </p>
                </div>
                <div class="mx-auto" style="margin-top: auto" >
                <a href="quotesform.php">   <button class="btn btn-info">GET A QUOTES</button> </a>
                </div>
            
           </div>
           <br> <br> <br> <br>
</div>
     
                    <nav class="navbar navbar-expand-sm">
                        <small>Copyright © 2019 Accountant | Powered by Accountant</small>
                        <ul class="navbar-nav ml-auto" >
                           
                        <li class="nav-item ">
                            <a href="Phomepage.php" class="nav-link " style="color: black">Home</a>
                            </li>
                            <li class="nav-item ">
                            <a href="about.php" class="nav-link " style="color: black">About</a>
                            </li>
                            <li class="nav-item ">
                            <a href="services.php" class="nav-link " style="color: black">Services</a>
                            </li>
                            <li class="nav-item ">
                            <a href="chooseme.php" class="nav-link " style="color: black">Why choose me</a>
                            </li>
                            <li class="nav-item ">
                            <a href="testimonial.php" class="nav-link " style="color: black">Testimonials</a>
                            </li>
                            <li class="nav-item ">
                            <a href="contact.php" class="nav-link " style="color: black">Contact</a>
                            </li>
                        </ul>
                        
    
                    </nav>
   
</body>
</html>